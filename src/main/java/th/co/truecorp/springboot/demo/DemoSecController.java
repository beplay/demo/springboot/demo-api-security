package th.co.truecorp.springboot.demo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class DemoSecController {

  @GetMapping
  public String publicHello() {
    return "Hello .";
  }
  
  /**
   * Sample method for protected with HMAC256
   * @return
   */
  @PostMapping(path = "/protected/dm", consumes = MediaType.TEXT_PLAIN_VALUE)
  public  String directMessage(@RequestBody String message) {
    return String.format("Hello, %s", message);
  }
  
  /**
   * Sample method for public 
   * @return
   */
  @PostMapping(path="/public/channel")
  public String getChannel() {
    return "Channel";
  }
}
